# Changelog

All notable changes to this project will be documented in this file.

## [0.1.1](https://gitlab.com/biomedit/s3event-webhook/-/releases/0%2E1%2E1) - 2025-02-18
[See all changes since the last release](https://gitlab.com/biomedit/s3event-webhook/compare/0%2E1%2E0...0%2E1%2E1)

### ✨ Features

- Handle object deletion events ([bde6bc3](https://gitlab.com/biomedit/s3event-webhook/commit/bde6bc3f129a1eff18dd7e53bb31fb4e6ed27d02)), Close #9

### 🐞 Bug Fixes

- **event:** Process all records in an event ([0511919](https://gitlab.com/biomedit/s3event-webhook/commit/051191973747030e9b24e06910023d2403987fe1))

### 🧱 Build system and dependencies

- Bump MSRV to 1.83 ([710d3be](https://gitlab.com/biomedit/s3event-webhook/commit/710d3be22ea5cb1667400fb1ea843bd3f15ca794))
- Bump axum to 0.8 ([9b4a1dd](https://gitlab.com/biomedit/s3event-webhook/commit/9b4a1dd7ea0ee8f94d8c6ea108143af082b8d56c)), Close #13
- Disable default features of aws-config to remove some unused dependencies ([4ecbf87](https://gitlab.com/biomedit/s3event-webhook/commit/4ecbf873ff73d9ea549c1c990b24c4bfab31cb98))
- **container:** Migrate to scratch base image + musl binary ([19a56f7](https://gitlab.com/biomedit/s3event-webhook/commit/19a56f715e10f791e2b563c2b82fd2f6f69aafdc))
- Add LICENSE ([e2b7a60](https://gitlab.com/biomedit/s3event-webhook/commit/e2b7a602da9c79411285c11f6731c8ac4e9f314d))
- **bumpversion:** Use git-cliff for calculating next version ([0b23d63](https://gitlab.com/biomedit/s3event-webhook/commit/0b23d6396d69688b1b57b892fa6423fba87f2582))

### 👷 CI

- **docker:** Publish `dev` releases from default branch ([25fe3a5](https://gitlab.com/biomedit/s3event-webhook/commit/25fe3a5b2eae6427a240387cf92ff84e5bc08053))
- **publish:** Use `publish-` prefix for publish stage jobs ([6b86d74](https://gitlab.com/biomedit/s3event-webhook/commit/6b86d740a1136a311a7d9992adc90abdcb07039f))
- Add cargo test job ([8efb6a9](https://gitlab.com/biomedit/s3event-webhook/commit/8efb6a9d07aa63fca49b8f5a74ca6fd452049bc5))
- Add check-build-min-versions job ([66a5d16](https://gitlab.com/biomedit/s3event-webhook/commit/66a5d169cec2ffa802a9edc72e48451676309f22)), Close #12

### 🧹 Refactoring

- Extract event parsing into a module ([be7dac4](https://gitlab.com/biomedit/s3event-webhook/commit/be7dac42c1c3b1a332581dde2ebc8a402183a88f))
- Use `sett` for processing data packages ([5d6a2e3](https://gitlab.com/biomedit/s3event-webhook/commit/5d6a2e3b46aa61a0997a46cb0fde86ecd3dad623)), Close #11

## [0.1.0](https://gitlab.com/biomedit/s3event-webhook/-/releases/0%2E1%2E0) - 2024-03-20

### ✨ Features

- Add `/health` endpoint ([de3dee2](https://gitlab.com/biomedit/s3event-webhook/commit/de3dee27b7a20702989b76316ae4e126b9f2d489)), Close #3
- Log the current version when the service starts ([0d5571e](https://gitlab.com/biomedit/s3event-webhook/commit/0d5571eced167c96f5460e94ba2cf1cbd1a791f9))
- Initial implementation ([01ef2ad](https://gitlab.com/biomedit/s3event-webhook/commit/01ef2ad4d63acbe3758bf962911961dccec1e50d))

### 🐞 Bug Fixes

- **logging:** Decrease the default logging verbosity ([2797f82](https://gitlab.com/biomedit/s3event-webhook/commit/2797f822abec00d3246c2aebe53ba26cd538078d))
- **zip:** Handle any central directory extra field ([b003623](https://gitlab.com/biomedit/s3event-webhook/commit/b0036232d3ff8c317a805f6410230b24b0afcd7d))

### 🧱 Build system and dependencies

- **docker:** Add HEALTHCHECK instruction to the image ([c933506](https://gitlab.com/biomedit/s3event-webhook/commit/c9335061ecf48a4df82b7b63ab3e7968a93b0f5c))
- Add bumpversion script ([fde1df3](https://gitlab.com/biomedit/s3event-webhook/commit/fde1df355ed1f9b769baea8712c5246c30f7662d)), Close #2
- Update lockfile ([f62fac7](https://gitlab.com/biomedit/s3event-webhook/commit/f62fac7eaef95c88f43450909b5986f0ebf0df4d))

### 👷 CI

- Switch from "branch" to "merge request" pipelines ([cf09621](https://gitlab.com/biomedit/s3event-webhook/commit/cf096218e059ade50c3e118b1d9a40a25447dff0))
- Publish artifacts ([fec3fbd](https://gitlab.com/biomedit/s3event-webhook/commit/fec3fbda350f6148ee3143313a7595fd8e8d78df)), Close #1
- Update renovate configuration ([7ab8969](https://gitlab.com/biomedit/s3event-webhook/commit/7ab896989b9cde19782b23080c24df6ddc3f88e4))
- Add renovate.json ([0e84ff5](https://gitlab.com/biomedit/s3event-webhook/commit/0e84ff5a2a44c66eead57abbc68e2f3f70a7e8e5))

### 📝 Documentation

- Add documentation on setting up a development environment ([a10b33e](https://gitlab.com/biomedit/s3event-webhook/commit/a10b33e32b6c40f9cbbfb31b170743a06ff9d78a)), Close #5
- Use AWS environmental variables instead of a credentials file ([0af1ac5](https://gitlab.com/biomedit/s3event-webhook/commit/0af1ac58e88ae453cff11e6a3c96ac54c8b0d466))
