# s3event-webhook

A webhook that listens for S3 events related to uploading
[data packages](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett/src/dpkg.rs)
and sends notifications to a dedicated [Portal](https://gitlab.com/biomedit/portal) API endpoint.

## Usage

`s3event-webhook` can be run as a standalone binary or as a Docker container.
It requires the following environment variables to be set:

- `AWS_ACCESS_KEY_ID` An S3 access key with the read access to all objects.
- `AWS_SECRET_ACCESS_KEY` An S3 secret key.
- `WEBHOOK_S3_ENDPOINT` S3 URL from which notifications are received.
- `WEBHOOK_NODE_CODE` Node code (unique identifier) for the current `s3event-webhook` instance.
- `WEBHOOK_PORTAL_ENDPOINT` Portal API endpoint for sending notifications
  (`https://<hostname>/backend/data-package/log/`).
- `WEBHOOK_PORTAL_USERNAME` Portal user for sending notifications with necessary permissions.
- `WEBHOOK_PORTAL_PASSWORD` Portal user password.

The following environment variables are _optional_:

- `WEBHOOK_PORT` (default: 5000) Port on which the webhook listens for S3 events.

Example values for the environment variables can be found in the `.env.sample` file.
An environmental variables file should be loaded using the `source .env` command,
before running the service.

Note: there are other methods for configuring S3 credentials. For more information
see [AWS documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html).

Run the standalone binary using the following command:

```bash
./s3event-webhook
```

The webhook currently exposes the following endpoints:

- `POST /event` - the main endpoint for receiving S3 events.
- `GET /health` - a simple health check endpoint. It returns an empty response
  with the 200 status code if the webhook service is running.

### Logging

By default, the service logs at the `info` level. The log level can be changed
by setting the `RUST_LOG` environment variable, e.g. to increase logging
verbosity set `RUST_LOG="s3event_webhook=trace,tower_http=debug"`

For more information see [tracing_subscriber documentation](https://docs.rs/tracing-subscriber/latest/tracing_subscriber/filter/struct.EnvFilter.html).

## Pre-build binaries

A standalone binary can be downloaded from the
[releases page](https://gitlab.com/biomedit/s3event-webhook/-/releases).

A Docker image can be pulled from the
[container registry](https://gitlab.com/biomedit/s3event-webhook/container_registry)
using the following command:

```bash
docker pull registry.gitlab.com/biomedit/s3event-webhook:0.1.0
```

Note: The version number should be replaced with the desired version.

## Building from source

A standalone binary can be built using the following command:

```bash
cargo build --release
```

A Docker image can be built using the following command:

```bash
docker build -t s3event-webhook .
```

## Development

See this [wiki page](https://gitlab.com/biomedit/s3event-webhook/-/wikis/Development)
for information on how to set up a development environment, which allows for end-to-end testing
