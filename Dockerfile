FROM docker.io/library/rust:1.85.0 AS build
ARG CARGO_BUILD_TARGET=x86_64-unknown-linux-musl

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get -y install capnproto musl-tools \
  && rustup target add ${CARGO_BUILD_TARGET}

COPY Cargo.* /build/
COPY src/ /build/src/

RUN cd /build && cargo install --path . --root /

FROM scratch

COPY --from=build /bin/s3event-webhook /bin/s3event-webhook
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

ENV WEBHOOK_PORT=5000

ENTRYPOINT ["/bin/s3event-webhook"]

HEALTHCHECK --timeout=3s \
  CMD ["/bin/s3event-webhook", "--health-check"]
