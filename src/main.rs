use std::sync::Arc;

use axum::{
    Router,
    routing::{get, post},
};
use tower_http::{timeout::TimeoutLayer, trace::TraceLayer};
use tracing::{debug, info};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod event;
mod handler;
mod portal;
mod state;

#[tokio::main]
async fn main() {
    let port = std::env::var("WEBHOOK_PORT")
        .map(|p| p.parse::<u16>().expect("failed parsing port number to u16"))
        .unwrap_or(5000);
    if std::env::args().nth(1).as_deref() == Some("--health-check") {
        if let Err(error) = health_check(port).await {
            eprintln!("Health check failed: {error}");
            std::process::exit(1);
        }
        return;
    }
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "s3event_webhook=info,tower_http=info".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();
    info!(
        "starting {} {}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );
    let state = Arc::new(
        crate::state::AppState::new()
            .await
            .expect("failed to create state"),
    );
    let app = Router::new()
        .route("/event", post(crate::handler::s3_event))
        .route("/health", get(crate::handler::health))
        .with_state(state)
        .layer((
            TraceLayer::new_for_http(),
            // Graceful shutdown will wait for outstanding requests to complete,
            // until the timeout is reached.
            TimeoutLayer::new(std::time::Duration::from_secs(10)),
        ));

    let listener = tokio::net::TcpListener::bind(("0.0.0.0", port))
        .await
        .expect("failed to bind to port");
    debug!("listening on {}", listener.local_addr().unwrap());
    axum::serve(listener, app)
        .with_graceful_shutdown(shutdown_signal())
        .await
        .expect("server failed");
}

async fn shutdown_signal() {
    let ctrl_c = async {
        tokio::signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        tokio::signal::unix::signal(tokio::signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        _ = ctrl_c => {},
        _ = terminate => {},
    }
}

async fn health_check(port: u16) -> Result<(), String> {
    let response = reqwest::get(format!("http://localhost:{port}/health"))
        .await
        .map_err(|e| e.to_string())?;
    let status = response.status();
    if !status.is_success() {
        return Err(format!(
            "{status}: {}",
            response.text().await.map_err(|e| e.to_string())?
        ));
    }
    Ok(())
}
