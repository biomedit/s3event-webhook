use anyhow::Context as _;
use tracing::{error, trace};

#[derive(Debug, serde::Serialize)]
pub(crate) struct Payload<'a> {
    pub(crate) file_name: &'a str,
    pub(crate) file_size: Option<u64>,
    pub(crate) metadata: Option<&'a str>,
    pub(crate) node: &'a str,
    pub(crate) status: PackageStatus,
}

#[derive(Clone, Copy, Debug, serde::Serialize)]
#[serde(rename_all = "UPPERCASE")]
pub(crate) enum PackageStatus {
    Processed,
    Deleted,
}

pub(crate) async fn send_package_notification(
    state: &crate::state::AppState,
    payload: Payload<'_>,
) -> anyhow::Result<()> {
    trace!(?payload, "sending notification to Portal");
    let response = state
        .http_client
        .post(&state.portal_endpoint)
        .header("Content-Type", "application/json")
        .basic_auth(&state.portal_username, Some(&state.portal_password))
        .body(serde_json::to_string(&payload)?)
        .send()
        .await
        .context("error while sending notification to portal")?;
    if !response.status().is_success() {
        let status = response.status().as_u16();
        let body = response
            .text()
            .await
            .context("failed to read response body")
            .unwrap_or_else(|e| format!("{:#}", e));
        error!(status, body, "failed to send notification to Portal");
        anyhow::bail!("error while sending notification to portal");
    }
    Ok(())
}
