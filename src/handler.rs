use std::sync::Arc;

use axum::{Json, extract::State, http::StatusCode, response::IntoResponse};
use tracing::{error, instrument, trace};

#[instrument(skip_all)]
pub(super) async fn s3_event(
    State(state): State<Arc<crate::state::AppState>>,
    Json(payload): Json<crate::event::Event>,
) -> impl IntoResponse {
    trace!(payload = ?payload, "received event");
    use crate::event::Record;
    for record in payload.records {
        // NOTE: in case that `s3:ObjectAccessed:Get` event needs to be handled,
        // it's necessary to ignore events generated by the webhook itself.
        // Otherwise it will enter an infinite loop.
        match record {
            Record::ObjectCreatedPut { s3 }
            | Record::ObjectCreatedCompleteMultipartUpload { s3 } => {
                let result = handle_object_created_event(&state, s3).await;
                if let Err(e) = result {
                    error!(error = format!("{:#}", e), "error");
                    return StatusCode::INTERNAL_SERVER_ERROR;
                }
            }
            Record::ObjectRemovedDelete { s3 } => {
                let result = handle_object_deleted_event(&state, s3).await;
                if let Err(e) = result {
                    error!(error = format!("{:#}", e), "error");
                    return StatusCode::INTERNAL_SERVER_ERROR;
                }
            }
            event => {
                trace!(?event, "event ignored");
                continue;
            }
        }
    }
    StatusCode::OK
}

async fn handle_object_created_event(
    state: &crate::state::AppState,
    s3: crate::event::S3ObjectCreated,
) -> anyhow::Result<()> {
    let metadata = sett::package::Package::open_s3(
        &state.s3client,
        s3.bucket.name.clone(),
        s3.object.key.clone(),
    )
    .await?
    .metadata_unverified()
    .await?;
    let metadata = serde_json::to_string(&metadata)?;
    let payload = crate::portal::Payload {
        file_name: &s3.object.key,
        file_size: Some(s3.object.size),
        metadata: Some(&metadata),
        node: &state.node_code,
        status: crate::portal::PackageStatus::Processed,
    };
    crate::portal::send_package_notification(state, payload).await
}

async fn handle_object_deleted_event(
    state: &crate::state::AppState,
    s3: crate::event::S3ObjectDeleted,
) -> anyhow::Result<()> {
    let payload = crate::portal::Payload {
        file_name: &s3.object.key,
        file_size: None,
        metadata: None,
        node: &state.node_code,
        status: crate::portal::PackageStatus::Deleted,
    };
    crate::portal::send_package_notification(state, payload).await
}

pub(super) async fn health() -> impl IntoResponse {
    StatusCode::OK
}
