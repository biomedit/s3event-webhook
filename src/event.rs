use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub(crate) struct Event {
    pub(crate) records: Vec<Record>,
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(tag = "eventName", rename_all = "camelCase")]
// We use event names with both `s3:` prefix and without it.
// This way we can support both AWS and MinIO events:
// the former doesn't use the prefix, while the latter does.
pub(crate) enum Record {
    #[serde(alias = "s3:TestEvent")]
    TestEvent,

    #[serde(alias = "ObjectCreated:Put", alias = "s3:ObjectCreated:Put")]
    ObjectCreatedPut { s3: S3ObjectCreated },

    #[serde(alias = "ObjectCreated:Post", alias = "s3:ObjectCreated:Post")]
    ObjectCreatedPost,

    #[serde(alias = "ObjectCreated:Copy", alias = "s3:ObjectCreated:Copy")]
    ObjectCreatedCopy,

    #[serde(
        alias = "ObjectCreated:CompleteMultipartUpload",
        alias = "s3:ObjectCreated:CompleteMultipartUpload"
    )]
    ObjectCreatedCompleteMultipartUpload { s3: S3ObjectCreated },

    #[serde(alias = "ObjectRemoved:Delete")]
    #[serde(alias = "s3:ObjectRemoved:Delete")]
    ObjectRemovedDelete { s3: S3ObjectDeleted },

    #[serde(
        alias = "ObjectRemoved:DeleteMarkerCreated",
        alias = "s3:ObjectRemoved:DeleteMarkerCreated"
    )]
    ObjectRemovedDeleteMarkerCreated,

    #[serde(alias = "ObjectRestore:Post", alias = "s3:ObjectRestore:Post")]
    ObjectRestorePost,

    #[serde(
        alias = "ObjectRestore:Completed",
        alias = "s3:ObjectRestore:Completed"
    )]
    ObjectRestoreCompleted,

    #[serde(alias = "ObjectRestore:Delete", alias = "s3:ObjectRestore:Delete")]
    ObjectRestoreDelete,

    #[serde(
        alias = "Replication:OperationFailedReplication",
        alias = "s3:Replication:OperationFailedReplication"
    )]
    ReplicationOperationFailedReplication,

    #[serde(
        alias = "Replication:OperationMissedThreshold",
        alias = "s3:Replication:OperationMissedThreshold"
    )]
    ReplicationOperationMissedThreshold,

    #[serde(
        alias = "Replication:OperationReplicatedAfterThreshold",
        alias = "s3:Replication:OperationReplicatedAfterThreshold"
    )]
    ReplicationOperationReplicatedAfterThreshold,

    #[serde(
        alias = "Replication:OperationNotTracked",
        alias = "s3:Replication:OperationNotTracked"
    )]
    ReplicationOperationNotTracked,

    #[serde(
        alias = "LifecycleExpiration:Delete",
        alias = "s3:LifecycleExpiration:Delete"
    )]
    LifecycleExpirationDelete,

    #[serde(
        alias = "LifecycleExpiration:DeleteMarkerCreated",
        alias = "s3:LifecycleExpiration:DeleteMarkerCreated"
    )]
    LifecycleExpirationDeleteMarkerCreated,

    #[serde(alias = "LifecycleTransition", alias = "s3:LifecycleTransition")]
    LifecycleTransition,

    #[serde(alias = "IntelligentTiering", alias = "s3:IntelligentTiering")]
    IntelligentTiering,

    #[serde(alias = "ObjectTagging:Put", alias = "s3:ObjectTagging:Put")]
    ObjectTaggingPut,

    #[serde(alias = "ObjectTagging:Delete", alias = "s3:ObjectTagging:Delete")]
    ObjectTaggingDelete,

    #[serde(alias = "ObjectAcl:Put", alias = "s3:ObjectAcl:Put")]
    ObjectAclPut,

    #[serde(other)]
    Unknown,
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct S3ObjectCreated {
    pub(crate) bucket: Bucket,
    pub(crate) object: ObjectCreated,
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct S3ObjectDeleted {
    pub(crate) bucket: Bucket,
    pub(crate) object: ObjectDeleted,
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct Bucket {
    pub(crate) name: String,
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct ObjectCreated {
    pub(crate) key: String,
    pub(crate) size: u64,
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct ObjectDeleted {
    pub(crate) key: String,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_put_event() {
        let event = r#"
            {
                "EventName": "s3:ObjectCreated:Put",
                "Key": "foo/20241021T082259.zip",
                "Records": [
                    {
                    "eventVersion": "2.0",
                    "eventSource": "minio:s3",
                    "awsRegion": "",
                    "eventTime": "2024-10-21T08:23:05.214Z",
                    "eventName": "s3:ObjectCreated:Put",
                    "userIdentity": {
                        "principalId": "minioadmin"
                    },
                    "requestParameters": {
                        "principalId": "minioadmin",
                        "region": "",
                        "sourceIPAddress": "[::1]"
                    },
                    "responseElements": {
                        "x-amz-id-2": "dd9025bab4ad464b049177c95eb6ebf374d3b3fd1af9251148b658df7ac2e3e8",
                        "x-amz-request-id": "18006A29EC5A4608",
                        "x-minio-deployment-id": "5f1a1bc5-2f48-4629-8fd8-e9d17f83f6c3",
                        "x-minio-origin-endpoint": "http://10.172.50.111:9000"
                    },
                    "s3": {
                        "s3SchemaVersion": "1.0",
                        "configurationId": "Config",
                        "bucket": {
                            "name": "foo",
                            "ownerIdentity": {
                                "principalId": "minioadmin"
                        },
                        "arn": "arn:aws:s3:::foo"
                        },
                        "object": {
                            "key": "20241021T082259.zip",
                            "size": 1944510,
                            "eTag": "da4ef861612e397064349415109e8d69",
                            "contentType": "application/octet-stream",
                            "userMetadata": {
                                "content-type": "application/octet-stream"
                        },
                        "sequencer": "18006A29ECFADB68"
                        }
                    },
                    "source": {
                        "host": "[::1]",
                        "port": "",
                        "userAgent": "aws-sdk-rust/1.3.3 os/macos lang/rust/1.82.0"
                    }
                    }
                ]
            }
        "#;
        let event: Event = serde_json::from_str(event).unwrap();
        assert_eq!(event.records.len(), 1);
        assert_eq!(
            event.records[0],
            Record::ObjectCreatedPut {
                s3: S3ObjectCreated {
                    bucket: Bucket {
                        name: "foo".to_string()
                    },
                    object: ObjectCreated {
                        key: "20241021T082259.zip".to_string(),
                        size: 1944510
                    }
                }
            }
        );
    }

    #[test]
    fn parse_delete_event() {
        let event = r#"
            {
            "EventName": "s3:ObjectRemoved:Delete",
            "Key": "foo/20241021T080323.zip",
            "Records": [
                {
                "eventVersion": "2.0",
                "eventSource": "minio:s3",
                "awsRegion": "",
                "eventTime": "2024-10-21T08:23:56.976Z",
                "eventName": "s3:ObjectRemoved:Delete",
                "userIdentity": {
                    "principalId": "minioadmin"
                },
                "requestParameters": {
                    "principalId": "minioadmin",
                    "region": "",
                    "sourceIPAddress": "127.0.0.1"
                },
                "responseElements": {
                    "content-length": "165",
                    "x-amz-id-2": "dd9025bab4ad464b049177c95eb6ebf374d3b3fd1af9251148b658df7ac2e3e8",
                    "x-amz-request-id": "18006A35FA3D9FC0",
                    "x-minio-deployment-id": "5f1a1bc5-2f48-4629-8fd8-e9d17f83f6c3",
                    "x-minio-origin-endpoint": "http://10.172.50.111:9000"
                },
                "s3": {
                    "s3SchemaVersion": "1.0",
                    "configurationId": "Config",
                    "bucket": {
                        "name": "foo",
                        "ownerIdentity": {
                            "principalId": "minioadmin"
                    },
                    "arn": "arn:aws:s3:::foo"
                    },
                    "object": {
                        "key": "20241021T080323.zip",
                        "sequencer": "18006A35FA5D7C00"
                    }
                },
                "source": {
                    "host": "127.0.0.1",
                    "port": "",
                    "userAgent": "MinIO (darwin; amd64) minio-go/v7.0.77 MinIO Console/(dev)"
                }
                }
            ]
            }
        "#;
        let event: Event = serde_json::from_str(event).unwrap();
        assert_eq!(event.records.len(), 1);
        assert_eq!(
            event.records[0],
            Record::ObjectRemovedDelete {
                s3: S3ObjectDeleted {
                    bucket: Bucket {
                        name: "foo".to_string()
                    },
                    object: ObjectDeleted {
                        key: "20241021T080323.zip".to_string()
                    }
                }
            }
        );
    }
}
