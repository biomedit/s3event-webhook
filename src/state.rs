pub(super) struct AppState {
    pub(super) s3client: sett::remote::s3::Client,
    pub(super) http_client: reqwest::Client,
    pub(super) node_code: String,
    pub(super) portal_endpoint: String,
    pub(super) portal_username: String,
    pub(super) portal_password: String,
}

fn load_from_env(key: &str) -> anyhow::Result<String> {
    std::env::var(key).map_err(|e| anyhow::anyhow!("{}: {}", key, e))
}

impl AppState {
    pub(super) async fn new() -> anyhow::Result<Self> {
        let s3client = sett::remote::s3::Client::builder()
            .region(Some("local"))
            .endpoint(Some(load_from_env("WEBHOOK_S3_ENDPOINT")?))
            .build()
            .await?;
        let http_client = reqwest::Client::new();
        Ok(Self {
            s3client,
            http_client,
            node_code: load_from_env("WEBHOOK_NODE_CODE")?,
            portal_endpoint: load_from_env("WEBHOOK_PORTAL_ENDPOINT")?,
            portal_username: load_from_env("WEBHOOK_PORTAL_USERNAME")?,
            portal_password: load_from_env("WEBHOOK_PORTAL_PASSWORD")?,
        })
    }
}
